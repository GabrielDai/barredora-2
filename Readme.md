<!--
SPDX-FileCopyrightText: 2022 Gabriel Moyano <vgmoyano@gmail.com>

SPDX-License-Identifier: MPL-2.0
-->

# Barredora-2

Rust package for controlling roombas compatible with [create 2 open interface](https://edu.irobot.com/learning-library/create-2-oi-spec).
