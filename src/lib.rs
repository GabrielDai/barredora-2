/*
 * SPDX-FileCopyrightText: 2022 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: MPL-2.0
 */

mod comm;
use crate::comm::messages::answers;
use crate::comm::messages::answers::Answer;
use crate::comm::messages::commands;
use crate::comm::messages::commands::Command;
use embedded_hal::serial::nb::{Write, Read};


#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Velocity {
    pub speed: i16,
    pub radius: i16,
}

pub enum Clockwise {
    Clockwise,
    CounterClockwise,
}

pub enum SongBank {
    Bank1 = 0,
    Bank2 = 1,
    Bank3 = 2,
    Bank4 = 3,
    Bank5 = 4,
}


pub struct Robot<T: Write + Read> {
    port: T,
}

impl<T: Write + Read> Robot<T> {
    pub fn create(port: T) -> Robot<T> {
        Robot{port}
    }

    fn send_cmd(&mut self, cmd: impl Command) -> Result<(), embedded_hal::nb::Error<T::Error>> {
        for byte in cmd.get_message().iter() {
            self.port.write(*byte)?;
        }
        Ok(())
    }

    fn read_answer(&mut self, answer: &mut impl Answer) -> Result<(), embedded_hal::nb::Error<T::Error>> {
        let buffer = answer.get_buffer();
        for i in 0..buffer.len() {
            let result = self.port.read();
            match result {
                Ok(read_byte) => {buffer[i] = read_byte;},
                Err(err) => return Err(err),
            }
        }
        answer.parse();
        Ok(())
    }

    pub fn start(&mut self) {
        let cmd = commands::Start::create();
        self.send_cmd(cmd).expect("Error Starting the robot");
    }

    pub fn stop(&mut self) {
        let cmd = commands::Stop::create();
        self.send_cmd(cmd).expect("Error Stoping the robot");
    }

    pub fn enter_full_mode(&mut self) {
        let cmd = commands::Full::create();
        self.send_cmd(cmd).expect("Error entering Full mode");
    }

    pub fn get_basic_sensors_state(&mut self) -> answers::EmergencyStop {
        let mut cmd = commands::Sensors::create();
        cmd.request_group(commands::GroupId::EmergencyStop);
        self.send_cmd(cmd).expect("Error Requesting the EmergencyStop variables");

        let mut answer = answers::EmergencyStop::create();
        self.read_answer(&mut answer).expect("Error reading answer from robot");
        answer
    }

    pub fn record_song(&mut self, bank: SongBank, song: &[u8]) {
        let mut cmd = commands::Song::create();
        cmd.save_song(bank, song);
        self.send_cmd(cmd).expect("Error Recording song");
    }

    pub fn play_song(&mut self, bank: SongBank) {
        let mut cmd = commands::Play::create();
        cmd.select_bank(bank);
        self.send_cmd(cmd).expect("Error Playing song");
    }

    pub fn update_velocity(&mut self, velocity: Velocity) {
        let mut cmd = commands::Drive::create();
        cmd.set_velocity(velocity);
        self.send_cmd(cmd).expect("Error Updating velocity");
    }

    pub fn update_speed(&mut self, speed: i16) {
        let mut cmd = commands::Drive::create();
        cmd.set_speed(speed);
        self.send_cmd(cmd).expect("Error Updating speed");
    }

    pub fn turn_in_place(&mut self, speed: i16, clockwise: Clockwise) {
        let mut cmd = commands::Drive::create();
        cmd.set_turn_in_place(speed, clockwise);
        self.send_cmd(cmd).expect("Error Turning in place");
    }
}

impl<T: Write + Read> Drop for Robot<T> {
    fn drop(&mut self) {
        self.stop();
    }
}
