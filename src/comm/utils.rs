/*
 * SPDX-FileCopyrightText: 2022 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: MPL-2.0
 */

#![allow(dead_code)]

pub fn write_i16(value: i16, s: &mut[u8]) {
    let value = value.to_be_bytes();
    for i in 0..s.len() {
        s[i] = value[i];
    }
}

pub fn read_i16(s: &[u8]) -> i16 {
    i16::from_be_bytes([s[0], s[1]])
}


#[cfg(test)]
mod tests {
    use crate::comm::utils;

    #[test]
    fn write_and_read_i16_correctly() {
        let original_value: i16 = -200;
        let mut bytes: [u8; 2] = [0, 0];
        utils::write_i16(original_value, &mut bytes[..]);

        let value = original_value.to_be_bytes();
        for i in 0..value.len() {
            assert_eq!(value[i], bytes[i]);
        }

        let value = utils::read_i16(&bytes[..]);
        assert_eq!(value, original_value);
    }
}
