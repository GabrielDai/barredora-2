/*
 * SPDX-FileCopyrightText: 2022 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: MPL-2.0
 */

type SimpleMsg = [u8; 1];
type OneByteMsg = [u8; 2];
type ThreeBytesMsg = [u8; 4];
type FourBytesMsg = [u8; 5];

pub mod commands;
pub mod answers;
