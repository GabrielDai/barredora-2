/*
 * SPDX-FileCopyrightText: 2023 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: MPL-2.0
 */

#![allow(dead_code)]

use intbits::Bits;


pub trait Answer {
    fn create() -> Self;
    fn parse(&mut self);
    fn get_buffer(&mut self) -> &mut[u8];
}


// Packet ID 7
#[derive(Debug)]
pub struct BumpsAndWheelDrops {
    buffer: [u8; 1],
    pub wheel_drop_left: bool,
    pub wheel_drop_right: bool,
    pub bump_left: bool,
    pub bump_right: bool,
}

impl Answer for BumpsAndWheelDrops {
    fn create() -> Self {
        BumpsAndWheelDrops {
            buffer: [0u8],
            wheel_drop_left: false,
            wheel_drop_right: false,
            bump_left: false,
            bump_right: false,
        }
    }

    fn parse(&mut self) {
        self.wheel_drop_left = self.buffer[0].bit(3);
        self.wheel_drop_right = self.buffer[0].bit(2);
        self.bump_left = self.buffer[0].bit(1);
        self.bump_right = self.buffer[0].bit(0);
    }

    fn get_buffer(&mut self) -> &mut[u8] {
        &mut self.buffer[..]
    }
}


// Packet ID 14
#[derive(Debug)]
pub struct Overcurrent {
    buffer: [u8; 1],
    pub left_wheel: bool,
    pub right_wheel: bool,
    pub main_brush: bool,
    pub side_brush: bool,
}

impl Answer for Overcurrent {
    fn create() -> Self {
        Overcurrent {
            buffer: [0u8],
            left_wheel: false,
            right_wheel: false,
            main_brush: false,
            side_brush: false,
        }
    }

    fn parse(&mut self) {
        self.left_wheel = self.buffer[0].bit(4);
        self.right_wheel = self.buffer[0].bit(3);
        self.main_brush = self.buffer[0].bit(2);
        self.side_brush = self.buffer[0].bit(0);
    }

    fn get_buffer(&mut self) -> &mut[u8] {
        &mut self.buffer[..]
    }
}


// Group ID 1 (Here named EmergencyStop)
#[derive(Debug)]
pub struct EmergencyStop {
    buffer: [u8; 10],
    pub bumps_and_wheels: BumpsAndWheelDrops,
    pub wall: bool,
    pub virtual_wall: bool,
    pub cliff_left: bool,
    pub cliff_right: bool,
    pub cliff_front_left: bool,
    pub cliff_front_right: bool,
    pub overcurrent: Overcurrent,
    pub dirt_level: u8,
}

impl Answer for EmergencyStop {
    fn create() -> Self {
        EmergencyStop {
            buffer: [0u8; 10],
            bumps_and_wheels: BumpsAndWheelDrops::create(),
            wall: false,
            virtual_wall: false,
            cliff_left: false,
            cliff_right: false,
            cliff_front_left: false,
            cliff_front_right: false,
            overcurrent: Overcurrent::create(),
            dirt_level: 0u8,
        }
    }

    fn parse(&mut self) {
        self.bumps_and_wheels.get_buffer()[0] = self.buffer[0];
        self.bumps_and_wheels.parse();
        self.wall = self.buffer[1] == 1;
        self.virtual_wall = self.buffer[6] == 1;
        self.cliff_left = self.buffer[2] == 1;
        self.cliff_right = self.buffer[5] == 1;
        self.cliff_front_left = self.buffer[3] == 1;
        self.cliff_front_right = self.buffer[4] == 1;
        self.overcurrent.get_buffer()[0] = self.buffer[7];
        self.overcurrent.parse();
        self.dirt_level = self.buffer[8];
    }

    fn get_buffer(&mut self) -> &mut[u8] {
        &mut self.buffer[..]
    }
}


#[cfg(test)]
mod tests {
    use crate::comm::messages::answers;
    use crate::comm::messages::answers::Answer;

    #[test]
    fn parse_bumps_and_wheel_drops() {
        let byte = 0b1010;
        let mut bumps_and_wheels = answers::BumpsAndWheelDrops::create();
        bumps_and_wheels.get_buffer()[0] = byte;
        bumps_and_wheels.parse();
        assert_eq!(bumps_and_wheels.wheel_drop_left, true);
        assert_eq!(bumps_and_wheels.wheel_drop_right, false);
        assert_eq!(bumps_and_wheels.bump_left, true);
        assert_eq!(bumps_and_wheels.bump_right, false);
    }

    #[test]
    fn parse_overcurrent() {
        let byte = 0b10101;
        let mut overcurrent = answers::Overcurrent::create();
        overcurrent.get_buffer()[0] = byte;
        overcurrent.parse();
        assert_eq!(overcurrent.left_wheel, true);
        assert_eq!(overcurrent.right_wheel, false);
        assert_eq!(overcurrent.main_brush, true);
        assert_eq!(overcurrent.side_brush, true);
    }

    #[test]
    fn parse_emergency_stop() {
        let bytes: [u8; 10] = [10, 1, 0, 1, 0, 1, 0, 21, 42, 0];
        let mut emergeny_stop = answers::EmergencyStop::create();
        let buffer = emergeny_stop.get_buffer();
        for i in 0..buffer.len() {
            buffer[i] = bytes[i];
        }
        emergeny_stop.parse();

        assert_eq!(emergeny_stop.bumps_and_wheels.wheel_drop_left, true);
        assert_eq!(emergeny_stop.bumps_and_wheels.wheel_drop_right, false);
        assert_eq!(emergeny_stop.bumps_and_wheels.bump_left, true);
        assert_eq!(emergeny_stop.bumps_and_wheels.bump_right, false);

        assert_eq!(emergeny_stop.wall, true);
        assert_eq!(emergeny_stop.virtual_wall, false);
        assert_eq!(emergeny_stop.cliff_left, false);
        assert_eq!(emergeny_stop.cliff_right, true);
        assert_eq!(emergeny_stop.cliff_front_left, true);
        assert_eq!(emergeny_stop.cliff_front_right, false);

        assert_eq!(emergeny_stop.overcurrent.left_wheel, true);
        assert_eq!(emergeny_stop.overcurrent.right_wheel, false);
        assert_eq!(emergeny_stop.overcurrent.main_brush, true);
        assert_eq!(emergeny_stop.overcurrent.side_brush, true);

        assert_eq!(emergeny_stop.dirt_level, 42);
    }
}
