/*
 * SPDX-FileCopyrightText: 2022 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: MPL-2.0
 */

#![allow(dead_code)]

use intbits::Bits;
use crate::Velocity;
use crate::Clockwise;
use crate::SongBank;
use crate::comm::utils;
use crate::comm::messages::SimpleMsg;
use crate::comm::messages::OneByteMsg;
use crate::comm::messages::ThreeBytesMsg;
use crate::comm::messages::FourBytesMsg;


enum CmdId {
    Reset = 7,
    Start = 128,
    Safe = 131,
    Full = 132,
    Power = 133,
    Spot = 134,
    Clean = 135,
    Max = 136,
    Drive = 137,
    Motors = 138,
    Leds = 139,
    Song = 140,
    Play = 141,
    Sensors = 142,
    SeekDock = 143,
    PWMMotors = 144,
    DriveDirect = 145,
    DrivePWM = 146,
    Stream = 148,
    QueryList = 149,
    PauseResumeStream = 150,
    Stop = 173,
}

pub trait Command {
    const ID: u8;

    fn create() -> Self;
    fn get_message(&self) -> &[u8];
}


pub struct SimpleCmd<const ID: u8> {
    message: SimpleMsg,
}

impl<const ID: u8> Command for SimpleCmd<ID> {
    const ID: u8 = ID;

    fn create() -> Self {
        SimpleCmd::<ID> {
            message: [ID],
        }
    }

    fn get_message(&self) -> &[u8] {
        &self.message[..]
    }
}

pub type Start = SimpleCmd::<{CmdId::Start as u8}>;
pub type Reset = SimpleCmd::<{CmdId::Reset as u8}>;
pub type Stop = SimpleCmd::<{CmdId::Stop as u8}>;
pub type Safe = SimpleCmd::<{CmdId::Safe as u8}>;
pub type Full = SimpleCmd::<{CmdId::Full as u8}>;
pub type Clean = SimpleCmd::<{CmdId::Clean as u8}>;
pub type Max = SimpleCmd::<{CmdId::Max as u8}>;
pub type Spot = SimpleCmd::<{CmdId::Spot as u8}>;
pub type SeekDock = SimpleCmd::<{CmdId::SeekDock as u8}>;
pub type Power = SimpleCmd::<{CmdId::Power as u8}>;


pub struct OneByteCmd<const ID: u8> {
    message: OneByteMsg,
}

impl<const ID: u8> Command for OneByteCmd<ID> {
    const ID: u8 = ID;

    fn create() -> Self {
        OneByteCmd::<ID> {
            message: [ID, 0],
        }
    }

    fn get_message(&self) -> &[u8] {
        &self.message[..]
    }
}

pub type Sensors = OneByteCmd::<{CmdId::Sensors as u8}>;
pub type Play = OneByteCmd::<{CmdId::Play as u8}>;
pub type Motors = OneByteCmd::<{CmdId::Motors as u8}>;
pub type PauseResumeStream = OneByteCmd::<{CmdId::PauseResumeStream as u8}>;


pub enum GroupId {
    EmergencyStop = 1,
}

impl Sensors {
    pub fn request_group(&mut self, group: GroupId) {
        self.message[1] = group as u8;
    }
}


impl Play {
    pub fn select_bank(&mut self, bank: SongBank) {
        self.message[1] = bank as u8;
    }
}


impl Motors {
    pub fn config_main_brush(&mut self, on: bool, flapper_inward: bool) {
        self.message[1].set_bit(2, on);
        self.message[1].set_bit(4, !flapper_inward);
    }

    pub fn config_side_brush(&mut self, on: bool, clockwise: Clockwise) {
        self.message[1].set_bit(0, on);
        let value: bool = match clockwise {
            Clockwise::Clockwise => true,
            Clockwise::CounterClockwise => false,
        };
        self.message[1].set_bit(3, value);
    }

    pub fn config_vacuum(&mut self, on: bool) {
        self.message[1].set_bit(1, on);
    }
}


impl PauseResumeStream {
    pub fn pause(&mut self) {
        self.message[1] = 0;
    }

    pub fn resume(&mut self) {
        self.message[1] = 1;
    }
}


pub struct ThreeBytesCmd<const ID: u8> {
    message: ThreeBytesMsg,
}

impl<const ID: u8> Command for ThreeBytesCmd<ID> {
    const ID: u8 = ID;

    fn create() -> Self {
        ThreeBytesCmd::<ID> {
            message: [ID, 0, 0, 0],
        }
    }

    fn get_message(&self) -> &[u8] {
        &self.message[..]
    }
}

pub type PWMMotors = ThreeBytesCmd::<{CmdId::PWMMotors as u8}>;
pub type Leds = ThreeBytesCmd::<{CmdId::Leds as u8}>;


impl PWMMotors {
    fn restrict_motors_pwm(pwm: i8) -> i8 {
        if pwm < -127 {
            return -127;
        }
        return pwm;
    }

    fn restrict_vacuum_pwm(pwm: u8) -> u8 {
        if pwm > 127 {
            return 127;
        }
        return pwm;
    }

    pub fn set_motors_pwm(&mut self, main_brush_pwm: i8, side_brush_pwm: i8, vacuum_pwm: u8) {
        let main_brush_pwm = Self::restrict_motors_pwm(main_brush_pwm);
        let side_brush_pwm = Self::restrict_motors_pwm(side_brush_pwm);
        let vacuum_pwm = Self::restrict_vacuum_pwm(vacuum_pwm);
        self.message[1] = main_brush_pwm.to_be_bytes()[0];
        self.message[2] = side_brush_pwm.to_be_bytes()[0];
        self.message[3] = vacuum_pwm;
    }
}


impl Leds {
    pub fn turn_leds_on(&mut self, check_robot: bool, dock: bool, spot: bool, debris: bool) {
        self.message[1].set_bit(0, debris);
        self.message[1].set_bit(1, spot);
        self.message[1].set_bit(2, dock);
        self.message[1].set_bit(3, check_robot);
    }

    pub fn set_power_led(&mut self, color: u8, intensity: u8) {
        self.message[2] = color;
        self.message[3] = intensity;
    }
}


pub struct FourBytesCmd<const ID: u8> {
    message: FourBytesMsg,
}

impl<const ID: u8> Command for FourBytesCmd<ID> {
    const ID: u8 = ID;

    fn create() -> Self {
        FourBytesCmd::<ID> {
            message: [ID, 0, 0, 0, 0],
        }
    }

    fn get_message(&self) -> &[u8] {
        &self.message[..]
    }
}

pub type Drive = FourBytesCmd::<{CmdId::Drive as u8}>;
pub type DriveDirect = FourBytesCmd::<{CmdId::DriveDirect as u8}>;
pub type DrivePWM = FourBytesCmd::<{CmdId::DrivePWM as u8}>;


fn restrict_speed(speed: i16) -> i16 {
    if speed < -500 {
        return -500;
    } else if speed > 500 {
        return 500;
    }
    return speed;
}

fn restrict_radius(radius: i16) -> i16 {
    if radius < -2000 {
        return -2000;
    } else if radius > 2000 {
        return 2000;
    }
    return radius;
}

impl Drive {
    pub fn set_velocity(&mut self, velocity: Velocity) {
        let speed = restrict_speed(velocity.speed);
        let radius = restrict_radius(velocity.radius);
        utils::write_i16(speed, &mut self.message[1..3]);
        utils::write_i16(radius, &mut self.message[3..]);
    }

    pub fn set_speed(&mut self, speed: i16) {
        let radius: i16 = 32767;
        let speed = restrict_speed(speed);
        utils::write_i16(speed, &mut self.message[1..3]);
        utils::write_i16(radius, &mut self.message[3..]);
    }

    pub fn set_turn_in_place(&mut self, speed: i16, clockwise: Clockwise) {
        let radius: i16 = match clockwise {
            Clockwise::Clockwise => -1,
            Clockwise::CounterClockwise => 1,
        };
        let speed = restrict_speed(speed);
        utils::write_i16(speed, &mut self.message[1..3]);
        utils::write_i16(radius, &mut self.message[3..]);
    }
}


impl DriveDirect {
    pub fn set_wheels_speed(&mut self, right_wheel_speed: i16, left_wheel_speed: i16) {
        let right_wheel_speed = restrict_speed(right_wheel_speed);
        let left_wheel_speed = restrict_speed(left_wheel_speed);
        utils::write_i16(right_wheel_speed, &mut self.message[1..3]);
        utils::write_i16(left_wheel_speed, &mut self.message[3..]);
    }
}


fn restrict_wheels_pwm(pwm: i16) -> i16 {
    if pwm < -255 {
        return -255;
    } else if pwm > 255 {
        return 255;
    }
    return pwm;
}

impl DrivePWM {
    pub fn set_wheels_pwm(&mut self, right_wheel_pwm: i16, left_wheel_pwm: i16) {
        let right_wheel_pwm = restrict_wheels_pwm(right_wheel_pwm);
        let left_wheel_pwm = restrict_wheels_pwm(left_wheel_pwm);
        utils::write_i16(right_wheel_pwm, &mut self.message[1..3]);
        utils::write_i16(left_wheel_pwm, &mut self.message[3..]);
    }
}


pub struct Song {
    message: [u8; 35],
    len: usize,
}

impl Command for Song {
    const ID: u8 = CmdId::Song as u8;

    fn create() -> Self {
        Song {
            message: [Song::ID, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            len: 2,
        }
    }

    fn get_message(&self) -> &[u8] {
        &self.message[..self.len]
    }
}

impl Song {
    pub fn save_song(&mut self, bank: SongBank, song: &[u8]) {
        self.len = 3 + song.len();
        self.message[1] = bank as u8;
        self.message[2] = song.len() as u8 / 2u8;
        for i in 0..song.len() {
            self.message[i + 3] = song[i];
        }
    }
}


pub struct DynamicCmd<const ID: u8> {
    message: Vec<u8>,
}

impl<const ID: u8> Command for DynamicCmd<ID> {
    const ID: u8 = ID;

    fn create() -> Self {
        DynamicCmd::<ID> {
            message: vec![ID, 0u8],
        }
    }

    fn get_message(&self) -> &[u8] {
        &self.message[..]
    }
}

impl<const ID: u8> DynamicCmd<ID> {
    pub fn add_packet(&mut self, packet: GroupId) {
        self.message.push(packet as u8);
        self.message[1] += 1;
    }
}

pub type Stream = DynamicCmd::<{CmdId::Stream as u8}>;
pub type QueryList = DynamicCmd::<{CmdId::QueryList as u8}>;


#[cfg(test)]
mod tests {
    use crate::Velocity;
    use crate::Clockwise;
    use crate::SongBank;
    use crate::comm::utils;
    use crate::comm::messages::commands;
    use crate::comm::messages::commands::Command;

    #[test]
    fn create_simple_cmd_correctly() {
        let cmd = commands::Start::create();
        assert_eq!(cmd.get_message()[0], commands::CmdId::Start as u8);
    }

    #[test]
    fn set_velocity_correctly() {
        let vel = Velocity{speed: -200, radius: 500};
        let mut cmd = commands::Drive::create();
        cmd.set_velocity(vel);

        let speed: i16 = utils::read_i16(&cmd.get_message()[1..3]);
        let radius = utils::read_i16(&cmd.get_message()[3..]);
        let config_vel = Velocity{speed, radius};
        assert_eq!(vel, config_vel);
    }

    #[test]
    fn set_speed_correctly() {
        let mut cmd = commands::Drive::create();

        let speed: i16 = -501;
        cmd.set_speed(speed);
        let set_speed: i16 = utils::read_i16(&cmd.get_message()[1..3]);
        assert_eq!(set_speed, -500);

        let speed: i16 = 501;
        cmd.set_speed(speed);
        let set_speed: i16 = utils::read_i16(&cmd.get_message()[1..3]);
        assert_eq!(set_speed, 500);

        let speed: i16 = 42;
        cmd.set_speed(speed);
        let set_speed: i16 = utils::read_i16(&cmd.get_message()[1..3]);
        assert_eq!(set_speed, speed);
        let set_radius: i16 = utils::read_i16(&cmd.get_message()[3..]);
        assert_eq!(set_radius, 32767);
    }

    #[test]
    fn turn_in_place_correctly() {
        let mut cmd = commands::Drive::create();
        let speed: i16 = 42;

        cmd.set_turn_in_place(speed, Clockwise::Clockwise);
        let set_radius: i16 = utils::read_i16(&cmd.get_message()[3..]);
        assert_eq!(set_radius, -1);

        cmd.set_turn_in_place(speed, Clockwise::CounterClockwise);
        let set_radius: i16 = utils::read_i16(&cmd.get_message()[3..]);
        assert_eq!(set_radius, 1);
    }

    #[test]
    fn sensor_request_group() {
        let mut cmd = commands::Sensors::create();
        cmd.request_group(commands::GroupId::EmergencyStop);
        assert_eq!(commands::CmdId::Sensors as u8, cmd.get_message()[0]);
        assert_eq!(commands::GroupId::EmergencyStop as u8, cmd.get_message()[1]);
    }

    #[test]
    fn create_song_cmd() {
        let mut cmd = commands::Song::create();
        let song: [u8; 10] = [1, 2, 3, 4, 5 ,6 ,7 ,8 ,9, 10];
        let bank = SongBank::Bank5;
        cmd.save_song(bank, &song);
        assert_eq!(cmd.get_message().len(), 13);
        assert_eq!(cmd.get_message()[0], commands::CmdId::Song as u8);
        assert_eq!(cmd.get_message()[1], SongBank::Bank5 as u8);
        assert_eq!(cmd.get_message()[2], song.len() as u8 / 2u8);
        for i in 3..cmd.get_message().len() {
            assert_eq!(cmd.get_message()[i], song[i - 3]);
        }
    }

    #[test]
    fn set_direct_drive_cmd() {
        let mut cmd = commands::DriveDirect::create();
        assert_eq!(cmd.get_message()[0], commands::CmdId::DriveDirect as u8);

        let speed: i16 = -501;
        cmd.set_wheels_speed(speed, speed);
        let right_wheel_speed: i16 = utils::read_i16(&cmd.get_message()[1..3]);
        let left_wheel_speed: i16 = utils::read_i16(&cmd.get_message()[3..]);
        assert_eq!(right_wheel_speed, -500);
        assert_eq!(left_wheel_speed, -500);

        let speed: i16 = 501;
        cmd.set_wheels_speed(speed, speed);
        let right_wheel_speed: i16 = utils::read_i16(&cmd.get_message()[1..3]);
        let left_wheel_speed: i16 = utils::read_i16(&cmd.get_message()[3..]);
        assert_eq!(right_wheel_speed, 500);
        assert_eq!(left_wheel_speed, 500);

        let speed: i16 = 42;
        cmd.set_wheels_speed(speed, speed);
        let right_wheel_speed: i16 = utils::read_i16(&cmd.get_message()[1..3]);
        let left_wheel_speed: i16 = utils::read_i16(&cmd.get_message()[3..]);
        assert_eq!(right_wheel_speed, speed);
        assert_eq!(left_wheel_speed, speed);
    }

    #[test]
    fn set_drive_pwm_cmd() {
        let mut cmd = commands::DrivePWM::create();
        assert_eq!(cmd.get_message()[0], commands::CmdId::DrivePWM as u8);

        let pwm: i16 = -256;
        cmd.set_wheels_pwm(pwm, pwm);
        let right_wheel_pwm: i16 = utils::read_i16(&cmd.get_message()[1..3]);
        let left_wheel_pwm: i16 = utils::read_i16(&cmd.get_message()[3..]);
        assert_eq!(right_wheel_pwm, -255);
        assert_eq!(left_wheel_pwm, -255);

        let pwm: i16 = 256;
        cmd.set_wheels_pwm(pwm, pwm);
        let right_wheel_pwm: i16 = utils::read_i16(&cmd.get_message()[1..3]);
        let left_wheel_pwm: i16 = utils::read_i16(&cmd.get_message()[3..]);
        assert_eq!(right_wheel_pwm, 255);
        assert_eq!(left_wheel_pwm, 255);

        let pwm: i16 = 42;
        cmd.set_wheels_pwm(pwm, pwm);
        let right_wheel_pwm: i16 = utils::read_i16(&cmd.get_message()[1..3]);
        let left_wheel_pwm: i16 = utils::read_i16(&cmd.get_message()[3..]);
        assert_eq!(right_wheel_pwm, pwm);
        assert_eq!(left_wheel_pwm, pwm);
    }

    #[test]
    fn config_motors() {
        let mut cmd = commands::Motors::create();
        cmd.config_main_brush(true, true);
        cmd.config_side_brush(true, Clockwise::Clockwise);
        assert_eq!(cmd.get_message()[0], commands::CmdId::Motors as u8);
        assert_eq!(cmd.get_message()[1], 13);
    }

    #[test]
    fn set_pwm_motors() {
        let mut cmd = commands::PWMMotors::create();
        assert_eq!(cmd.get_message()[0], commands::CmdId::PWMMotors as u8);

        cmd.set_motors_pwm(-128, -128, 255);
        assert_eq!(cmd.get_message()[1], (-127i8).to_be_bytes()[0]);
        assert_eq!(cmd.get_message()[2], (-127i8).to_be_bytes()[0]);
        assert_eq!(cmd.get_message()[3], 127u8);

        cmd.set_motors_pwm(-42, 42, 10);
        assert_eq!(cmd.get_message()[1], (-42i8).to_be_bytes()[0]);
        assert_eq!(cmd.get_message()[2], 42i8.to_be_bytes()[0]);
        assert_eq!(cmd.get_message()[3], 10u8);
    }

    #[test]
    fn config_leds() {
        let mut cmd = commands::Leds::create();
        assert_eq!(cmd.get_message()[0], commands::CmdId::Leds as u8);

        cmd.turn_leds_on(false, true, false, false);
        assert_eq!(cmd.get_message()[1], 4u8);
        let color: u8 = 0; // green
        let intensity: u8 = 128;
        cmd.set_power_led(color, intensity);
        assert_eq!(cmd.get_message()[2], 0u8);
        assert_eq!(cmd.get_message()[3], 128u8);
    }

    #[test]
    fn pause_resume_cmd() {
        let mut cmd = commands::PauseResumeStream::create();
        assert_eq!(cmd.get_message()[0], commands::CmdId::PauseResumeStream as u8);
        assert_eq!(cmd.get_message()[1], 0u8); // Pause by default

        cmd.resume();
        assert_eq!(cmd.get_message()[1], 1u8);
        cmd.pause();
        assert_eq!(cmd.get_message()[1], 0u8);
    }

    #[test]
    fn create_dynamic_cmd() {
        let mut cmd = commands::Stream::create();
        assert_eq!(cmd.get_message()[0], commands::CmdId::Stream as u8);
        assert_eq!(cmd.get_message()[1], 0u8);

        cmd.add_packet(commands::GroupId::EmergencyStop);
        assert_eq!(cmd.get_message()[1], 1u8);
        assert_eq!(cmd.get_message()[2], commands::GroupId::EmergencyStop as u8);
    }
}
