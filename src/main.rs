/*
 * SPDX-FileCopyrightText: 2022 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: MPL-2.0
 */

use barredora;
use linux_embedded_hal::Serial;
use linux_embedded_hal::serial_core::{SerialDevice, SerialPortSettings, BaudRate};
use std::{thread, time};


fn main() {
    let mut port = Serial::open("/dev/ttyUSB0").expect("The serial port couldn't be open");
    let mut port_settings = port.0.read_settings().expect("Error while reading port settings");
    port_settings.set_baud_rate(BaudRate::Baud115200).expect("Error while setting port configurations");
    port.0.write_settings(&port_settings).expect("Error while writting port configurations");

    let mut robot = barredora::Robot::create(port);
    robot.start();
    robot.enter_full_mode();

    let mario_bros: [u8; 24] = [76, 12, 76, 12, 20, 12, 76, 12, 20, 12, 72, 12, 76, 12, 20, 12, 79, 12, 20, 36, 67, 12, 20, 36];
    robot.record_song(barredora::SongBank::Bank1, &mario_bros);

    let imperial_march: [u8; 22] = [55, 48, 55, 48, 55, 48, 0, 2, 51, 32, 58, 16, 55, 48, 0, 2, 51, 32, 58, 16, 55, 48];
    robot.record_song(barredora::SongBank::Bank2, &imperial_march);

    let sleep_duration = time::Duration::from_millis(100);
    let mut sensors_state = robot.get_basic_sensors_state();
    while !sensors_state.bumps_and_wheels.wheel_drop_left && !sensors_state.bumps_and_wheels.wheel_drop_right {
        if sensors_state.bumps_and_wheels.bump_left {
            robot.play_song(barredora::SongBank::Bank1);
        } else if sensors_state.bumps_and_wheels.bump_right {
            robot.play_song(barredora::SongBank::Bank2);
        }

        thread::sleep(sleep_duration);
        sensors_state = robot.get_basic_sensors_state();
    }
}
